#!/bin/bash

chgrp paludisbuild /dev/tty

cave resolve repository/ruby -x1
cave generate-metadata
cave resolve maruku -x

export LC_ALL="en_US.UTF-8"
make
make deploy IMAGE="public"

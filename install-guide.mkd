Title: Install Guide
CSS: /css/main.css

#{include head}

Installation Guide
==================

Exherbo doesn't recommend particular filesystems or installation procedures but
the following is a simple example of how to install Exherbo. You can customise
the process any way you like from changing the choice of filesystems to more
advanced installations including LVM2 or preparing for diskless installations.

1. Read the documentation

    * Re-read everything on [paludis.exherbo.org][1] and [exherbo.org][2].
    * Check [recent mailing list posts][3] for any possible issues, especially
      paludis upgrade information

2. Boot a live system

    * Download [SystemRescueCD][4] (it has both 32 and 64 bit support)

          # links http://www.sysresccd.org/Download

    * Burn it to a CD or use [UNetbootin][5] to put it on a USB stick
    * Reboot, choose the right kernel, which is the alternative one, otherwise sydbox doesn't work, and get your network up

          # net-setup wlan0

3. Prepare the hard disk

    * Using BIOS/Legacy booting:

    * Create a boot partition (~500MB), a root partition (>=4GB),
      and (optionally) a home partition

          # cfdisk /dev/sda

    * Format the filesystems for each partition (asumming that sda1
      is boot, sda2 root and sda3 home). You can use file systems other
      than ext4 for your root and home partitons (e.g. btrfs)

          # mkfs.ext2 /dev/sda1
          # mkfs.ext4 /dev/sda2
          # mkfs.ext4 /dev/sda3

    * Using UEFI booting:

    * Create an ESP partition (~50MB should be plenty),
    a root partition (>=4GB) and (optionally) a home partition

          # cfdisk /dev/sda

    * Format the filesystems for each partition (asumming that sda1
    is boot, sda2 root and sda3 home). You can use file systems other
    than ext4 for everything but the ESP partition, which has to
    be FAT32 for UEFI systems.

          # mkfs.vfat -F32 /dev/sda1
          # mkfs.ext4 /dev/sda2
          # mkfs.ext4 /dev/sda3

    * Mount root and `cd` into it

          # mkdir /mnt/exherbo && mount /dev/sda2 /mnt/exherbo && cd /mnt/exherbo

    * Get the latest automatically-built daily archive of Exherbo from [Stages][6]
      and verify the consistence of the file:

          # curl -O https://dev.exherbo.org/stages/exherbo-x86_64-pc-linux-gnu-current.tar.xz
          # curl -O https://dev.exherbo.org/stages/exherbo-x86_64-pc-linux-gnu-current.tar.xz.sha256sum
          # sha256sum -c exherbo-x86_64-pc-linux-gnu-current.tar.xz.sha256sum

    * Extract the stage

          # tar xJpf exherbo*xz

    * Update fstab

          # cat <<EOF > /mnt/exherbo/etc/fstab
          # <fs>       <mountpoint>    <type>    <opts>      <dump/pass>
          /dev/sda2    /               ext4      defaults    0 0
          /dev/sda3    /home           ext4      defaults    0 2
          EOF

    * If you use BIOS/Legacy booting:

          # echo "/dev/sda1    /boot           ext2      defaults    0 0" >> /mnt/exherbo/etc/fstab

    * If you use UEFI booting:

          # echo "/dev/sda1    /boot           vfat      defaults    0 0" >> /mnt/exherbo/etc/fstab

4. Chroot into the system

    * Mount everything for the chroot

          # mount -o rbind /dev /mnt/exherbo/dev/

      **_IMPORTANT:_** Take a closer look that you really use the "rbind" option to bind mount /dev, otherwise
      paludis will not work properly.

          # mount -o rbind /sys /mnt/exherbo/sys/
          # mount -t proc none /mnt/exherbo/proc/
          # mount /dev/sda1 /mnt/exherbo/boot/
          # mount /dev/sda3 /mnt/exherbo/home/

    * Make sure the network can resolve DNS

          # cp /etc/resolv.conf /mnt/exherbo/etc/resolv.conf

    * Change your root

          # env -i TERM=$TERM SHELL=/bin/bash HOME=$HOME $(which chroot) /mnt/exherbo /bin/bash
          # source /etc/profile
          # export PS1="(chroot) $PS1"

5. Update the install

    * Make sure Paludis is configured correctly

          # cd /etc/paludis && vim bashrc && vim *conf

    * Sync all the trees -- now it is safe to sync

          # cave sync

6. Make it bootable

    * Download and extract the latest stable kernel from [The Linux Kernel Archives][7].

    * Install the kernel

          # cd path-to kernel
          # make nconfig
          # make
          # make modules_install
          # cp arch/x86/boot/bzImage /boot/kernel

    * Install an init system

      The glibc stages include the systemd init system. Although you can choose whatever
      init system (or none) you'd like to use.

      Here's a [HowTo][10] for installing systemd on Exherbo; if you would prefer to not use systemd, look [here][14].

      The systemd option is enabled by default in profiles and packages will be installed with
      systemd support after `cave sync`.

      You may want to re-install the packages included in the stage:

          # cave resolve world -c

      This will pick up the changes and re-install affected packages.

      Make sure to re-install systemd in order to create a valid machine-id:

          # cave resolve --execute --preserve-world --skip-phase test sys-apps/systemd

    * For BIOS/Legacy booting, using GRUB 2:

          # grub-install /dev/sda

    * Configure GRUB 2

           # cat<<EOF > /boot/grub/grub.cfg
           set timeout=10
           set default=0
           menuentry "Exherbo" {
               set root=(hd0,1)
               linux /kernel root=/dev/sda2
           }
           EOF

        **OR**

          # grub-mkconfig -o /boot/grub/grub.cfg

        **_IMPORTANT:_** `grub-mkconfig` requires that your kernel be named
        {kernel,vmlinuz}-version.  This will be the case if you `make install` after
        building your kernel.

    * For UEFI booting, using systemd-boot and kernel-install.
      Keep in mind that you can use other bootloaders, like GRUB2 or rEFInd too,
      please see their manuals for instructions on how to install them:

    * Install systemd-boot:

          # bootctl install

	* You have to install sys-apps/systemd with the efi option enabled,
          to use this command.

	* In case this doesn't work on your machine you may have to use efibootmgr
          or the likes and do the setup manually.

    * Copy the kernel you just installed into the appropriate directory
      and generate a initramfs for it if you have sys-boot/dracut installed:

          # kernel-install add kernel-version /boot/vmlinuz-kernel-version

    * To remove old kernel versions:

          # kernel-install remove kernel-version

    * Configure your hostname.

          # cat <<EOF > /etc/hostname
          my-hostname
          EOF

    * Make sure your hostname is mapped to localhost in /etc/hosts,
      otherwise some packages test suites will fail because of network sandboxing.

          # cat<<EOF > /etc/hosts
          127.0.0.1    my-hostname.domain.foo    my-hostname    localhost
          ::1          localhost
          EOF

    * If you need additional kernel support e.g. for an Intel wireless card install the Linux
      firmware files

          # cave resolve linux-firmware

    * Set root password

          # passwd

    * `glibc` only: Install any additional locales you might need.

      The locales `en_GB.UTF-8` and `en_US.UTF-8` are installed by default.

          # localedef -i en_US -f ISO-8859-1 en_US
          # localedef -i ru_RU -f UTF-8 ru_RU.UTF-8

    * And change the LANG environment variable if you prefer a different system wide locale than the
      default `en_GB.UTF-8`

          # echo LANG="en_US.UTF-8" > /etc/env.d/99locale

    * Set system timezone, e.g. to Copenhagen in Denmark, Europe

          # ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime

    * Reboot

          # reboot

7. Post-installation Tasks

    * Remove the stage tarball

        The stage tarball is no longer needed and can be safely removed.

    * Clean up packages

        The installation images (stages) contain additional tools which are useful for the
        installation process but are not part of the system nor world sets but the stages set.

        You can identify the additional packages using cave show:

            # cave show stages

        If you wish to remove them, you can simply use cave purge:

            # cave purge

        Alternatively, you can add packages you wish to retain to the world set by using the
        update-world command.  As an example, the following adds gdb to the world set.

            # cave update-world sys-devel/gdb

        Or, if you want to add all packages of the stages set to the world set:

            # cave update-world -s stages

    * Add a new user for daily use.

        Some useful groups with a brief description:

            - Administrative Groups:
                + adm   used for system monitoring tasks (e.g. journalctl)
                + users traditional group for identifying user accounts (versus system/daemon accounts)
                + wheel used to indicate permission to perform certain restricted operations (e.g.  su)

             - Device Access Groups:
                + disk  allows access to various disk device nodes
                + usb   allows access to usb device nodes
                + audio allows access to audio device nodes
                + video allows access to video device nodes (for accelerated video)


            # useradd -m -G adm,disk,wheel,cdrom,audio,video,usb,users ${username}

[1]: http://paludis.exherbo.org/
[2]: //exherbo.org/
[3]: http://lists.exherbo.org/pipermail/exherbo-dev/
[4]: http://www.sysresccd.org/
[5]: http://unetbootin.sourceforge.net/
[6]: http://dev.exherbo.org/stages/
[7]: //kernel.org/
[8]: //www.gnu.org/software/grub/
[9]: //exherbo.org/docs/faq.html
[10]: //exherbo.org/docs/systemd.html
[14]: //exherbo.org/docs/without-systemd.html

--
Copyright 2008, 2009, 2010 Jonathan Dahan

Copyright 2012 Saleem Abdulrasool

Copyright 2018 Rasmus Thomsen

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
